$( document ).ready(function() {
    $(".dathang").click(function (){
        $(".form-order-room").slideToggle();
        return "ok";
    });
    $(".language").hover(function (){
        $(".dropdown-language").slideToggle();
        return "ok";
    });
    $( ".datepicker" ).datepicker({
        minDate: '0',
        dateFormat: 'dd/mm/yy'
    });
    $( ".datepicker_arr" ).change(function(){
        var current_date = $( ".datepicker_arr" ).datepicker( "getDate" );
        $( ".datepicker_dep" ).datepicker( "option", "minDate", current_date );
    });
    $('[data-toggle="tooltip"]').tooltip();

    // Hien thi thong tin user khi dang nhap
    var access_token = localStorage.getItem("access_token");
    if(typeof(access_token) != "undefined" && access_token){
        var url = "http://api.nhatrang.local/api/getAuthenticatedUser";
        $.ajax({
            beforeSend: function(xhrObj){
                xhrObj.setRequestHeader("Accept","application/json");
            },
            url: url,
            headers: {
                'Authorization': 'Bearer ' + access_token
            },
            type: 'GET',
            cache: false,
            success: function (data){
                $(".message .title").html("<div onclick='profile()'>"+ data.user.user +"</div> / <div onclick='logout()'>logout</div>");
            },
        });
    }
});
// type: Int -> kiem tra so, Email -> kiem tra email, Phone ->kiem tra slideTogglebnty
function validateForm(target, required = false, type = false, min = false, max = false){
    $(target).removeClass("boder-danger");
    $(target).attr("data-original-title", "");
    var value = $(target).val();
    if(required){
        if(value == ""){
            $(target).addClass("boder-danger");
            $(target).attr("data-original-title", "Do not empty!");
            $(target).tooltip("show");
            return false;
        }
    }
    if(value != ""){
        if(type == "Int"){
            if(isNaN(parseInt(value))){
                $(target).addClass("boder-danger");
                $(target).attr("data-original-title", "Please enter the number!");
                $(target).hover();
                $(target).tooltip("show");
                return false;
            }
            if($.type(max) == 'number'){
                if(parseInt(value) > max){
                    $(target).addClass("boder-danger");
                    $(target).attr("data-original-title", "Please enter the number of max = "+max+" !");
                    $(target).tooltip("show");
                    return false;
                }
            }
            if($.type(min) == 'number'){
                if(parseInt(value) < min){
                    $(target).addClass("boder-danger");
                    $(target).attr("data-original-title", "Please enter the number of min = "+min+" !");
                    $(target).tooltip("show");
                    return false;
                }
            }
        } else {
            if($.type(max) == 'number'){
                if(value.length > max){
                    $(target).addClass("boder-danger");
                    $(target).attr("data-original-title", "Please enter the text of max = "+max+" characters!");
                    $(target).tooltip("show");
                    return false;
                }
            }
            if($.type(min) == 'number'){
                if(value.length < min){
                    $(target).addClass("boder-danger");
                    $(target).attr("data-original-title", "Please enter the text of min = "+min+" characters!");
                    $(target).tooltip("show");
                    return false;
                }
            }
        }
        if(type == "email"){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!re.test(value)){
                $(target).addClass("boder-danger");
                $(target).attr("data-original-title", "Email address isn't correct. Please try again!");
                $(target).tooltip("show");
                return false;
            }
        }
        if(type == "sdt"){
            var re = /^[0-9]{10,11}$/;
            if (!re.test(value)){
                $(target).addClass("boder-danger");
                $(target).attr("data-original-title", "Phone number isn't correct. Please try again!");
                $(target).tooltip("show");
                return false;
            }
        }
    }
    return true;
}
function validate(target){
    var check = $(target).find(".check");
    $(check).each(function (){
        $(this)[0].onkeyup();
    });
    var danger = $(target).find(".boder-danger");
    if(danger.length != 0){
        alert("Vui lòng nhập đầy đủ thông tin!");
        return false;
    }
    else {
        return true;
    }
}

function register(){
    var target = $('.form-register');
    if(validate(target)){
        $($(target).find(".show-errors")).each(function (){
            $(this).find(".errors").html("");
        });
        $($(target).find(".btn-submit")).attr("disabled", true);
        var url = "http://api.nhatrang.local/api/user";
        var user = $($(target).find("input[name='user']")).val();
        var email = $($(target).find("input[name='email']")).val();
        var password = $($(target).find("input[name='password']")).val();
        var rePassword = $($(target).find("input[name='rePassword']")).val();
        $.ajax({
            beforeSend: function(xhrObj){
                xhrObj.setRequestHeader("Accept","application/json");
            },
            url: url,
            type: 'POST',
            cache: false,
            data : {"user":user, "email":email, "password":password, "rePassword":rePassword},
            success: function (data){
                $(".form-register:last-child").replaceWith( "<div class='show-success col-xs-12 col-md-6'><h3>Tạo tài khoản thành công!</h3><div>" );
                $($(target).find(".btn-submit")).attr("disabled", false);
            },
            error: function (data){
                var errors = data.responseJSON;
                $.each(errors, function (key, value){
                    $('.form-register .'+ key +'-errors').html(value);
                    $('.form-register .'+ key +'-errors').parent().parent().css("display", "block");
                });
                $($(target).find(".btn-submit")).attr("disabled", false);
            }
        });
    }
}

function login(){
    var target = $('.form-login');
    if(validate(target)){
        $($(target).find(".btn-submit")).attr("disabled", true);
        $($(target).find(".show-errors")).each(function (){
            $(this).find(".errors").html("");
        });
        var url = "http://api.nhatrang.local/api/login";
        var user = $($(target).find("input[name='user']")).val();
        var password = $($(target).find("input[name='password']")).val();
        $.ajax({
            beforeSend: function(xhrObj){
                xhrObj.setRequestHeader("Accept","application/json");
            },
            url: url,
            type: 'POST',
            cache: false,
            data : {"user":user, "password":password},
            success: function (data){
                localStorage.setItem("access_token", data.token);
                window.location.href = "/";
            },
            error: function (data){
                if(data.status == 404){
                    $('.form-login .user-errors').html($.parseJSON(data.responseText).message);
                    $('.form-login .user-errors').parent().parent().css("display", "block");
                }
                if(data.status == 406){
                    $('.form-login .password-errors').html($.parseJSON(data.responseText).message);
                    $('.form-login .password-errors').parent().parent().css("display", "block");
                }
                $($(target).find(".btn-submit")).attr("disabled", false);
            }
        });
    }
}

function logout(){
    var access_token = localStorage.getItem("access_token");
    if(typeof(access_token) != "undefined"){
        var url = "http://api.nhatrang.local/api/logout";
        $.ajax({
            beforeSend: function(xhrObj){
                xhrObj.setRequestHeader("Accept","application/json");
            },
            url: url,
            headers: {
                'Authorization': 'Bearer ' + access_token
            },
            type: 'GET',
            cache: false,
            success: function (data){
                localStorage.removeItem("access_token");
                $(".message .title").html("<a href='index.php?a=login-register'>Login / Register</a>");
            },
        });
    }
}

function check_booking(){
    var access_token = localStorage.getItem("access_token");
    var url = "http://api.nhatrang.local/api/getAuthenticatedUser";
    $.ajax({
        beforeSend: function(xhrObj){
            xhrObj.setRequestHeader("Accept","application/json");
        },
        url: url,
        headers: {
            'Authorization': 'Bearer ' + access_token
        },
        type: 'GET',
        cache: false,
        success: function (data){
            var arrivalDate = $(".form-order-room input[name='dataBookingArrivaldate']").val();
            var departureDate = $(".form-order-room input[name='dataBookingDeparturedate']").val();
            var children = $(".form-order-room select[name='dataBookingChildren']").val();
            var adult =$(".form-order-room select[name='dataBookingAdult']").val();
            window.location.href = "index.php?a=form-booking&arrivalDate="+arrivalDate+"&departureDate="+departureDate+"&children="+children+"&adult="+adult;
        },
        error: function (data){
            window.location.href = "index.php?a=login-register&alert=true";
        }
    });
}

function profile(){
    var access_token = localStorage.getItem("access_token");
    var url = "http://api.nhatrang.local/api/getAuthenticatedUser";
    $.ajax({
        beforeSend: function(xhrObj){
            xhrObj.setRequestHeader("Accept","application/json");
        },
        url: url,
        headers: {
            'Authorization': 'Bearer ' + access_token
        },
        type: 'GET',
        cache: false,
        success: function (data){

            window.location.href = "index.php?a=profile&user="+data.user.user+"&email="+data.user.email;
        },
        error: function (data){
            window.location.href = "index.php?a=login-register";
        }
    });
}

function update_user(user){
    var target = $(".form-update");
    if(validate(target)){
        $($(target).find(".show-errors")).each(function (){
            $(this).find(".errors").html("");
        });
        $($(target).find(".btn-submit")).attr("disabled", true);
        var url = "http://api.nhatrang.local/api/user/"+user;
        var email = $($(target).find("input[name='email']")).val();
        var password = $($(target).find("input[name='password']")).val();
        var rePassword = $($(target).find("input[name='rePassword']")).val();
        var access_token = localStorage.getItem("access_token");
        $.ajax({
            beforeSend: function(xhrObj){
                xhrObj.setRequestHeader("Accept","application/json");
            },
            url: url,
            headers: {
                'Authorization': 'Bearer ' + access_token
            },
            type: 'PUT',
            cache: false,
            data : {"email":email, "password":password, "rePassword":rePassword},
            success: function (data){
                logout();
                window.location.href = "index.php?a=login-register&alert=2";
            },
            error: function (data){
                // console.log(data.responseText);
                if($.parseJSON(data.responseText).message == "Xác nhận mật khẩu không đúng"){
                    $('.form-update .rePassword-errors').html($.parseJSON(data.responseText).message);
                    $('.form-update .rePassword-errors').parent().parent().css("display", "block");
                } else {
                    logout();
                    window.location.href = "/";
                }
            }
        });
    }
}


