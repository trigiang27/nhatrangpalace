<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="public/css/bootstrap.min.css">
		<!-- Reset CSS -->
		<link rel="stylesheet" href="public/css/normalize.css">
		<!-- My CSS -->
		<link rel="stylesheet" href="public/css/my.css">
		<!-- jQuery UI Datepicker -->
		<link rel="stylesheet" href="public/libraries/jquery-ui-1.12.1.custom/jquery-ui.css">
		<!-- jQuery -->
		<script type="text/javascript" src="public/js/jquery.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<?php
		// header
			require("application/views/block/header.php");
		// navbar
			require("application/views/block/navbar.php");
		// content
			if(isset($_GET["a"])){
				$a = $_GET["a"];
				require("application/views/pages/$a.php");
			} else {
				require("application/views/pages/home.php");
			}
			if(isset($_GET["m"])){
				$m = $_GET["m"];
				require("application/manager/$m.php");
			}
		// follow
			require("application/views/block/follow.php");
		// footer
			require("application/views/block/footer.php");
		// message
			require("application/views/block/login.php");
		?>
		<!-- Bootstrap JavaScript -->
		<script type="text/javascript" src="public/js/bootstrap.min.js"></script>
		<!-- myscript.js -->
		<script type="text/javascript" src="public/js/myscript.js"></script>
		<!-- jQuery UI Datepicker -->
		<script src="public/libraries/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
	</body>
</html>
