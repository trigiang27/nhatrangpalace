Đây là  website nhatrangpalace:
1. Xây dựng giao diện trang Nha Trang Palace Hotel.
2. Xây dựng chức năng:
- Đặt phòng.
- Xây dựng function validate form booking.
- Lưu dữ liệu:
    + Lưu dữ liệu đặt phòng vào một file JSON.
    + Gửi email thông báo đặt phòng.
    + Lưu dữ liệu đặt phòng vào database.
- Quản lý đăng nhập: sử dụng API được cung cấp từ api.nhatrang để quản lý đăng nhập
    + Yêu cầu đăng nhập trước khi đặt phòng.
    + Đăng ký.
    + Đăng nhập.
    + Đổi mật khẩu.
