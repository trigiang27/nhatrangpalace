<?php
    include("public/libraries/PHPMailer-master/class.smtp.php");
    include "public/libraries/PHPMailer-master/class.phpmailer.php";
    include "public/libraries/PHPMailer-master/PHPMailerAutoload.php";

///////////////
// JSON file //
///////////////
if(isset($_POST)){
    $path = "application/data/data-booking.json";
    $new_data = [
        'Booking Information'=>
        [
            'Loại phòng'=>$_POST["dataBookingTyperoom"],
            'Số Phòng'=>$_POST["dataBookingRooms"],
            'Trẻ em'=>$_POST["dataBookingChildren"],
            'Người lớn'=>$_POST["dataBookingAdult"],
            'Ngày đến'=>$_POST["dataBookingArrivaldate"],
            'Ngày đi'=>$_POST["dataBookingDeparturedate"],
            'Ghi chú'=>$_POST["dataBookingNote"]
        ],
        'Thông tin cá nhân'=>
        [
            'Họ & Tên'=>$_POST["dataBookingName"],
            'Giới tính'=>$_POST["dataBookingGender"],
            'Địa chỉ'=>$_POST["dataBookingAddress"],
            'Thành phố'=>$_POST["dataBookingCity"],
            'Quốc tịch'=>$_POST["dataBookingNationality"],
            'Email'=>$_POST["dataBookingEmail"],
            'Điện thoại'=>$_POST["dataBookingPhone"]
        ]
    ];
    if(!is_dir('application/data')){
        mkdir('application/data');
    }
    if(!file_exists($path) || filesize($path) == 0){
        $file_open = fopen($path, "w");
        $data = [];
        array_push($data,$new_data);
        $json_data = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
        fwrite($file_open, $json_data);
        fclose($file_open);
    } else {
        $file_open = fopen($path, "r");
        $data = fread($file_open, filesize($path));
        $data = json_decode($data,true);
        fclose($file_open);
        $file_open = fopen($path, "w");
        array_push($data,$new_data);
        $json_data = json_encode($data, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
        fwrite($file_open, $json_data);
        fclose($file_open);
    }
    echo '<script>alert("Đặt phòng thành công!");</script>';

///////////////
// send mail //
///////////////

    $mail = new PHPMailer;
    // $mail->SMTPDebug = 2;
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'nguyentrigiang19911@gmail.com';    // SMTP username
    $mail->Password = 'bkwwwoxlvdppqscs';                   // SMTP password
    $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to
    $mail->setFrom('nguyentrigiang19911@gmail.com', 'Admin');
    $mail->addAddress('nguyentrigiang19911@gmail.com', 'Me');     // Add a recipient
    $mail->addReplyTo('nguyentrigiang19911@gmail.com', 'Reply');

    $mail->Subject = 'Booking';
    $new_data = "Booking Information:
    Loại phòng: ".$_POST["dataBookingTyperoom"]."
    Số Phòng: ".$_POST["dataBookingRooms"]."
    Trẻ em: ".$_POST["dataBookingChildren"]."
    Người lớn: ".$_POST["dataBookingAdult"]."
    Ngày đến: ".$_POST["dataBookingArrivaldate"]."
    Ngày đi: ".$_POST["dataBookingDeparturedate"]."
    Ghi chú: ".$_POST["dataBookingNote"]."
Thông tin cá nhân:"."
    Họ & Tên: ".$_POST["dataBookingName"]."
    Giới tính: ".$_POST["dataBookingGender"]."
    Địa chỉ: ".$_POST["dataBookingAddress"]."
    Thành phố: ".$_POST["dataBookingCity"]."
    Quốc tịch: ".$_POST["dataBookingNationality"]."
    Email: ".$_POST["dataBookingEmail"]."
    Điện thoại: ".$_POST["dataBookingPhone"];
    $mail->Body = $new_data;

    if(!$mail->Send()) {
        var_dump($mail->ErrorInfo);die();
        echo '<script>alert('.$mail->ErrorInfo.');</script>';
    } else {
        echo '<script>alert("Gửi mail thành công!");</script>';
    }
}

///////////////////
// save database //
///////////////////

try{
    $conn = new PDO("mysql:host=localhost;dbname=nhatrang;charset=utf8;", 'root', 'root');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    if($e->getMessage() === "SQLSTATE[HY000] [1049] Unknown database 'nhatrang'"){
        try{
            $conn = new PDO("mysql:host=localhost;charset=utf8;", 'root', 'root');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "CREATE DATABASE nhatrang
                        CHARACTER SET utf8
                        COLLATE utf8_general_ci;";
            $conn->exec($sql);
            $conn = null;
            $conn = new PDO("mysql:host=localhost;dbname=nhatrang;charset=utf8;", 'root', 'root');
            $sql = "CREATE TABLE type_room(
                        id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                        name VARCHAR(50) NOT NULL
                    )";
            $conn->exec($sql);
            $sql = "INSERT INTO type_room (name) VALUES ('Superior Cityview Room'), ('Palace Club Suite'), ('Superior Seaview Room'), ('Palace Suite'), ('Deluxe Room')";
            $conn->exec($sql);
            echo '<script>alert("Khởi tạo không thành công!");</script>';
        }
        catch (PDOException $e) {
           echo $e->getMessage();
        }
    }
}
$sql = "CREATE TABLE IF NOT EXISTS booking(
            id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
            fullName VARCHAR(50) NOT NULL,
            gender INT(1) UNSIGNED NOT NULL,
            address VARCHAR(50) NOT NULL,
            city VARCHAR(50),
            country VARCHAR(50),
            email VARCHAR(50) NOT NULL,
            phone INT(11) NOT NULL,
            typeRoom_id INT(5) NOT NULL,
            rooms INT(5) NOT NULL,
            children INT(5) NOT NULL,
            adult INT(5) NOT NULL,
            arrivalDate DATE NOT NULL,
            departureDate DATE,
            note VARCHAR(1000),
            addDate TIMESTAMP
        )";
$conn->exec($sql);
if(isset($_POST)){
        $stmt = $conn->prepare("INSERT INTO booking (fullName, gender, address, city, country, email, phone, typeRoom_id, rooms, children, adult, arrivalDate, departureDate, note) VALUES (:fullName, :gender, :address, :city, :country, :email, :phone, :typeRoom_id, :rooms, :children, :adult, :arrivalDate, :departureDate, :note)");
        $stmt->bindParam(':fullName', $_POST["dataBookingName"]);
        $stmt->bindParam(':gender', $_POST["dataBookingGender"]);
        $stmt->bindParam(':address', $_POST["dataBookingAddress"]);
        $stmt->bindParam(':city', $_POST["dataBookingCity"]);
        $stmt->bindParam(':country', $_POST["dataBookingNationality"]);
        $stmt->bindParam(':email', $_POST["dataBookingEmail"]);
        $stmt->bindParam(':phone', $_POST["dataBookingPhone"]);
        $stmt->bindParam(':typeRoom_id', $_POST["dataBookingTyperoom"]);
        $stmt->bindParam(':rooms', $_POST["dataBookingRooms"]);
        $stmt->bindParam(':children', $_POST["dataBookingChildren"]);
        $stmt->bindParam(':adult', $_POST["dataBookingAdult"]);
        //Doi format ngay
        $_POST["dataBookingArrivaldate"] = str_replace("/", "-", $_POST["dataBookingArrivaldate"]);
        $dataBookingArrivaldate = date("Y/m/d", strtotime($_POST["dataBookingArrivaldate"]));
        $stmt->bindParam(':arrivalDate', $dataBookingArrivaldate);
        //Doi format ngay
        $_POST["dataBookingDeparturedate"] = str_replace("/", "-", $_POST["dataBookingDeparturedate"]);
        $dataBookingDeparturedate = date("Y/m/d", strtotime($_POST["dataBookingDeparturedate"]));
        $stmt->bindParam(':departureDate', $dataBookingDeparturedate);
        $stmt->bindParam(':note', $_POST["dataBookingNote"]);
        $stmt->execute();
        echo '<script>alert("Lưu vào database thành công!");</script>';
}
$conn = null;
?>

