<div class="footer">
	<div class="container">
		<div class="body-footer row">
			<div class="col-sm-1"></div>
			<div class="hotel col-sm-1">
				<div class="row">
					<div class="bottom-head col-xs-12">
						<b><a href="#">Hotel</a></b>
					</div>
					<ul class="col-xs-12">
						<li>
							<a href="">Booking</a>
						</li>
						<li>
							<a href="">About Us</a>
						</li>
						<li>
							<a href="">Location</a>
						</li>
						<li>
							<a href="">Contact</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="accommodation col-sm-2">
				<div class="row">
					<div class="bottom-head col-xs-12">
						<b><a href="#">Accommodation</a></b>
					</div>
					<ul class="col-xs-12">
						<li>
							<a href="">Room</a>
						</li>
						<li>
							<a href="">Equipment</a>
						</li>
						<li>
							<a href="">Services</a>
						</li>
						<li>
							<a href="">Safely</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="services col-sm-2">
				<div class="row">
					<div class="bottom-head col-xs-12">
						<b><a href="#">Services</a></b>
					</div>
					<ul class="col-xs-12">
						<li>
							<a href="gioi-thieu">Accommodation</a>
						</li>
						<li>
							<a href="lien-he">Conferences</a>
						</li>
						<li>
							<a href="">Gastronomy</a>
						</li>
						<li>
							<a href="">Relaxation</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="menu col-sm-2">
				<div class="row">
					<div class="bottom-head col-xs-12">
						<b><a href="#">Menu</a></b>
					</div>
					<ul class="col-xs-12">
						<li>
							<a href="gioi-thieu">Menu Restaurace Prominent</a>
						</li>
						<li>
							<a href="lien-he">Business menu</a>
						</li>
						<li>
							<a href="">Lobby bar menu</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="contact-us col-sm-3">
				<div class="row">
					<div class="bottom-head col-xs-12">
						<b><a href="#">Contact Us</a></b>
					</div>
					<ul class="col-xs-12">
						<li>
							<a href="gioi-thieu">15 B Nguyen Thien Thuat, Nha Trang</a>
						</li>
						<li>
							<a href="lien-he">Tell: +84.510.3864610</a>
						</li>
						<li>
							<a href="">Fax: +84.510.3862334</a>
						</li>
						<li>
							<a href="">Location on Google map</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-1"></div>
		</div>
	</div>
	<div class="bottom-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-1"></div>
				<div class="payment col-sm-7 col-sm-push-4 col-md-6 col-md-push-5">
					<img src="public/image/paypal.png" alt="">
					<img src="public/image/visa.png" alt="">
					<img src="public/image/express.png" alt="">
					<img src="public/image/master.png" alt="">
				</div>
				<div class="col-sm-4 col-sm-pull-7 col-md-5 col-md-pull-6">
					<div class="info-website">Copyright © 2013 Vitour - All Rights Reseved</div>
				</div>
			</div>
		</div>
	</div>
</div>