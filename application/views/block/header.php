<div class="top">
    <div class="container">
        <div class="top-head">
            <h1 class="logo">
                <a href="index.php?a=home">
                    <img src="public/image/logo.png" alt="">
                </a>
            </h1>
            <div class="logo-small">
                <a href="index.php?a=home">
                    <img src="public/image/logo-180.png" alt="">
                </a>
            </div>
            <div class="aside-head">
                <div class="form-search">
                    <form action="#" method="GET" role="search">
                        <button class="btn btn-search" type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                        <input type="text" value="" name="query" placeholder="SEARCH">
                    </form>
                </div>
                <div class="list-language">
                    <div class="language">
                        <img src="public/image/eng.png" alt="">
                        English
                        <div class="dropdown-language">
                            <div>
                                <img src="public/image/vietnam.png" alt="">
                                VietNamese
                            </div>
                            <div>
                                <img src="public/image/eng.png" alt="">
                                English
                            </div>
                            <div>
                                <img src="public/image/russia.png" alt="">
                                Russia
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
