<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="btn btn-default dathang">
                Đặt phòng
            </div>
            <!-- <a class="navbar-brand" href="#">Title</a> -->
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php?a=home" class="active">Trang chủ</a></li>
                <li><a href="#">Giới thiệu</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Loại phòng</a>
                    <ul class="dropdown-menu">
                        <?php
                            try{
                                $conn = new PDO("mysql:host=localhost;dbname=nhatrang", 'root', 'root');
                                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $stmt = $conn->prepare("SELECT * FROM type_room");
                                $stmt->execute();
                                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                                $result = $stmt->fetchAll();
                                foreach($result as $typeRoom){
                                    echo '<li><a href="#">'.$typeRoom["name"].'</a></li>';
                                }
                            }
                            catch (PDOException $e) {
                                echo "Kết nối thất bại: " . $e->getMessage();
                            }
                        ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dịch vụ</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Phòng thể dục</a></li>
                        <li><a href="#">"Forever Young" Spa</a></li>
                        <li><a href="#">Private beach</a></li>
                        <li><a href="#">Swimming pool</a></li>
                        <li><a href="#">Tham Quan Du Lịch</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Nhà hàng - Bar</a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Nhà Hàng Royal</a></li>
                        <li><a href="#">Nhà Hàng Silver Sea</a></li>
                        <li><a href="#">Nhà Hàng Palace</a></li>
                        <li><a href="#">Palace Cafe</a></li>
                        <li><a href="#">Lobby Bar</a></li>
                        <li><a href="#">Moon Bar</a></li>
                    </ul>
                </li>
                <li class="hinhanh">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hình ảnh</a>
                </li>
                <li class="tintuc">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tin tức</a>
                </li>
                <li class="tuyendung"><a href="#">Tuyển dụng</a></li>
                <li class="lienhe"><a href="#">Liên hệ</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
