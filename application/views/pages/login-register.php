<div class="content">
    <div class="container">
        <div class="page-booking">
            <div class="header-box">
                <h2 class="title-box">ĐĂNG NHẬP / ĐĂNG KÝ</h2>
            </div>
            <div class="content-box">
                <div class="row">
                    <div class="form-login">
                        <div class="booking-info col-xs-12 col-md-6">
                            <h3>Đăng nhập</h3>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors token-errors col-xs-9">
                                        <?php
                                            if($_GET['alert'] == true){
                                                echo '<script>$(".form-login .token-errors").html("Vui lòng đăng nhập trước khi đặt phòng"); $(".form-login .token-errors").parent().parent().css("display", "block");</script>';
                                            }
                                            if($_GET['alert'] == 1){
                                                echo '<script>$(".form-login .token-errors").html("Bạn đã hết phiên làm việc vui lòng đăng nhập lại"); $(".form-login .token-errors").parent().parent().css("display", "block");</script>';
                                            }
                                            if($_GET['alert'] == 2){
                                                echo '<script>$(".form-login .token-errors").html("Vui lòng đăng nhập lại"); $(".form-login .token-errors").parent().parent().css("display", "block");</script>';
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Tên đăng nhập</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="user" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors user-errors col-xs-9">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Mật khẩu</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="password" class="check" type="password" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors password-errors col-xs-9">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="group-btn btn-login col-xs-9 col-xs-push-3">
                                    <button class="btn btn-submit" onclick="login()">Đăng nhập</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-register">
                        <div class="person-info col-xs-12 col-md-6">
                            <h3>Đăng ký</h3>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Tên đăng nhập</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="user" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,false,5,30)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors user-errors col-xs-9">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Email</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="email" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,"email",8,50)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Mật khẩu</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="password" class="check" type="password" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true, false, 8, 30)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Xác nhận mật khẩu</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="rePassword" class="check" type="password" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors rePassword-errors col-xs-9">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="group-btn btn-register col-xs-9 col-xs-push-3">
                                    <button class="btn btn-submit" onclick="register()">Đăng ký</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

