<div class="content">
    <div class="container">
        <div class="page-booking">
            <div class="header-box">
                <h2 class="title-box">ĐẶT PHÒNG</h2>
            </div>
            <div class="content-box">
                <div class="row">
                    <form action="index.php?m=booking" method="POST" onsubmit="return validate(this)">
                        <div class="booking-info col-xs-12 col-md-6">
                            <h3>Booking Information</h3>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Loại phòng</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <select name="dataBookingTyperoom">
                                            <?php
                                                try{
                                                    $conn = new PDO("mysql:host=localhost;dbname=nhatrang", 'root', 'root');
                                                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                                    $stmt = $conn->prepare("SELECT * FROM type_room");
                                                    $stmt->execute();
                                                    $stmt->setFetchMode(PDO::FETCH_ASSOC);
                                                    $result = $stmt->fetchAll();
                                                    foreach($result as $typeRoom){
                                                        echo '<option value="'.$typeRoom["id"].'">'.$typeRoom["name"].'</option>';
                                                    }
                                                }
                                                catch (PDOException $e) {
                                                    echo "Kết nối thất bại: " . $e->getMessage();
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Số Phòng</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingRooms" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,"Int",1)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="item col-xs-6">
                                    <div class="row">
                                        <div class="item-title required col-xs-6">
                                            <span>Trẻ em</span>
                                        </div>
                                        <div class="item-info col-xs-6">
                                            <input name="dataBookingChildren" class="check" value="<?php if(isset($_GET['children'])){echo $_GET['children'];}else{echo 0;}?>" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,"Int",0)'/>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-xs-6">
                                    <div class="row">
                                        <div class="item-title required col-xs-6">
                                            <span>Người lớn</span>
                                        </div>
                                        <div class="item-info col-xs-6">
                                            <input name="dataBookingAdult" class="check" value="<?php if(isset($_GET['adult'])){echo $_GET['adult'];}else{echo 1;}?>" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,"Int",1)'/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Ngày đến</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingArrivaldate" class="datepicker check datepicker_arr" type="text" value="<?php if(isset($_GET['arrivalDate'])){echo $_GET['arrivalDate'];}?>" data-toggle="tooltip" data-placement="top" readonly="true" onkeyup='validateForm(this,true)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                        <span>Ngày đi</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingDeparturedate" class="datepicker datepicker_dep" type="text" value="<?php if(isset($_GET['departureDate'])){echo $_GET['departureDate'];}?>" data-toggle="tooltip" data-placement="top" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row item">
                                    <div class="item-title col-xs-3">
                                        <span>Ghi chú</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <textarea name="dataBookingNote" cols="30" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="person-info col-xs-12 col-md-6">
                            <h3>Thông tin cá nhân</h3>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Họ & Tên</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingName" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,false,5,50)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Giới tính</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <select name="dataBookingGender">
                                            <option value="1">Nam</option>
                                            <option value="2">Nữ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row item">
                                    <div class="item-title required col-xs-3">
                                        <span>Địa chỉ</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingAddress" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,false,5,100)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                        <span>Thành phố</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingCity" type="text"/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                        <span>Quốc tịch</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingNationality" type="text"/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Email</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingEmail" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,"email")'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Điện thoại</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="dataBookingPhone" class="check" type="text" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true,"sdt")'/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="group-btn col-xs-12">
                            <input type="submit" class="btn-submit" value="Đặt phòng"/>
                            <input type="reset" class="btn-reset" value="Nhập lại"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

