<div class="content">
    <div class="container">
        <div class="page-booking">
            <div class="header-box">
                <h2 class="title-box">HỒ SƠ</h2>
            </div>
            <div class="content-box">
                <div class="row">
                    <div class="form-update">
                        <div class="person-info col-xs-12">
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Tên đăng nhập</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="user" class="check" type="text" data-toggle="tooltip" data-placement="top" value="<?php if(isset($_GET['user'])){echo $_GET['user'];}?>" onkeyup='validateForm(this,true,false,5,30)' disabled="true" />
                                    </div>
                                </div>
                            </div>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors user-errors col-xs-9">
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Email</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="email" class="check" type="text" data-toggle="tooltip" data-placement="top" value="<?php if(isset($_GET['email'])){echo $_GET['email'];}?>" onkeyup='validateForm(this,true,"email",8,50)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Mật khẩu</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="password" class="check" type="password" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true, false, 8, 30)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="item-title required col-xs-3">
                                        <span>Xác nhận mật khẩu</span>
                                    </div>
                                    <div class="item-info col-xs-9">
                                        <input name="rePassword" class="check" type="password" data-toggle="tooltip" data-placement="top" onkeyup='validateForm(this,true)'/>
                                    </div>
                                </div>
                            </div>
                            <div class="show-errors" style="display: none;">
                                <div class="row">
                                    <div class="item-title col-xs-3">
                                    </div>
                                    <div class="item-info errors rePassword-errors col-xs-9">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="group-btn btn-register col-xs-9 col-xs-push-3">
                                    <button class="btn btn-submit" onclick="update_user(<?php if(isset($_GET['user'])){echo "'".$_GET['user']."'";}?>)">Cập nhật</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

