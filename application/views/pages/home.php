<div class="banner">
    <img src="public/image/banner.png" alt="">
    <div class="container">
        <div class="form-order-room">
            <h2>Đặt phòng:</h2>
            <div class="form-horizontal">
                <div class="row-fluid">
                    <div class="day-group">
                        <input type="text" class="datepicker datepicker_arr" name="dataBookingArrivaldate" readonly="true" placeholder="Ngày đến">
                    </div>
                    <div class="day-group">
                        <input type="text" class="datepicker datepicker_dep" class="" name="dataBookingDeparturedate" readonly="true" placeholder="Ngày đi">
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="select-group">
                        <select name="dataBookingAdult">
                            <option value="0">-- Người lớn --</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                    <div class="select-group">
                        <select name="dataBookingChildren">
                            <option value="0">-- Trẻ em --</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                        </select>
                    </div>
                </div>
                <div class="button-form">
                    <button class="btn btn-submit" onclick="check_booking()">Đặt phòng</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="thump">
                <div class="col-xs-3">
                    <div class="item">
                        <img src="public/image/pic1.png" alt="">
                        <div class="title-thump">
                            <p>NHÀ HÀNG ROYAL RESTUANRANT</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="item">
                        <img src="public/image/pic2.png" alt="">
                        <div class="title-thump">
                            <p>NHÀ HÀNG ROYAL PALACE</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="item">
                        <img src="public/image/pic3.png" alt="">
                        <div class="title-thump">
                            <p>THE MOON BAR</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="item">
                        <img src="public/image/pic4.png" alt="">
                        <div class="title-thump">
                            <p>BEACH BAR "FANTASY"</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
